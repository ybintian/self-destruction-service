require_relative './control'

class Server
  def initialize
    run
  end

  def log_path
    File.join(File.dirname(__FILE__), "base_service.log")
  end

  def run
    file = File.open(log_path, 'a')
    count = 0
    loop do
      count += 1
      file.write("#{Time.now.to_i.to_s}\n")
      sleep 5
      raise 'byebye' if count > 3
    end
    file.close
  end
end

if __FILE__ == $0
  control = Control.new $log
  if ARGV[0] == "start"
    control.start
  elsif ARGV[0] == "stop"
    control.stop
  elsif ARGV[0] == "restart"
    control.stop
    control.start
  end
end
