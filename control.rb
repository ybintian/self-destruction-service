class Control
  def pid
    "/tmp/pull_gps_service.pid"
  end

  def initialize(log)
    @log = log
  end

  def start
    puts("#{Time.now} started")

    if File.exists?(pid)
      puts "#{pid} exists"
      exit
    end

    File.open(pid, "w") { |f| f.print Process.pid }
    Server.new
  end

  def stop
    unless File.exist?(pid)
      puts "#{pid} not exists"
      exit
    end

    ct = File.read(pid)
    File.delete pid
    cmd = "kill -9 #{ct}"
    puts cmd
    `#{cmd}`
  end
end

